class ParkEventRecover:
    def __init__(self, cur):
        self.cur = cur

    def recover_park_events(self):
        stmt = """SELECT system_id, bike_id, check_out_sample_id, check_in_sample_id
            FROM
            recover_park_events
        """
        self.cur.execute(stmt)
        for to_recover in self.cur.fetchall():
            self.reset_completed_park_event(to_recover[1], to_recover[2])
        stmt2 = """DELETE FROM recover_park_events"""
        self.cur.execute(stmt2)


    def reset_completed_park_event(self, bike_id, check_out_sample_id):
        print("Reset " + bike_id)
        print("Check_out_sample_id " + str(check_out_sample_id))
        stmt = """UPDATE
            park_events
            SET 
            check_out_sample_id = NULL,
            end_time = NULL
            WHERE bike_id = %s
            AND check_out_sample_id = %s
        """
        self.cur.execute(stmt, (bike_id, check_out_sample_id))

    
