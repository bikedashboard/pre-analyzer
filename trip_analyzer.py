from geopy.distance import distance

class Trip():
    def __init__(self, system_id, bike_id, start_lat, start_lng, start_time):
        self.system_id = system_id
        self.bike_id = bike_id
        self.start_lat = start_lat
        self.start_lng = start_lng
        self.start_time = start_time
        self.end_lat = None
        self.end_lng = None
        self.end_time = None

    def set_end_trip(self, other):
        self.end_lat = other["lat"]
        self.end_lng = other["lng"]
        self.end_time = other["last_time_imported"]

    def distance(self, other_lat, other_lng):
        return distance((self.start_lat, self.start_lng), (other_lat, other_lng)).m 

    def save(self, cur):
        stmt = """INSERT INTO trips
            (system_id, bike_id, start_location, end_location, start_time, end_time)
            VALUES (%s, %s, ST_SetSRID(ST_Point(%s, %s),4326), ST_SetSRID(ST_Point(%s, %s),4326),
            %s, %s)
        """
        cur.execute(stmt, (self.system_id, self.bike_id, self.start_lng, self.start_lat, 
            self.end_lng, self.end_lat, self.start_time, self.end_time))


class TripDeriver():
    def __init__(self, cur):
        self.cur = cur

    def analyze_records(self, records):
        trip = None
        bike_id = None
        highest_sample_id = 0
        for record in records:
            if record["is_assigned_to_trip"] == True:
                continue

            if trip:
                trip.set_end_trip(record)
                trip.save(self.cur)
                trip = None
                bike_id = record["bike_id"]
                highest_sample_id = record["sample_id"]

            # Overwrite trip when no movement is made.
            if record["is_check_out"]:
                trip = Trip(record["system_id"], record["bike_id"],
                    record["lat"], record["lng"], 
                    record["last_time_imported"])
        
        if bike_id and highest_sample_id > 0:
            self.set_samples_assigned(bike_id, highest_sample_id)

    def set_samples_assigned(self, bike_id, sample_id):
        stmt = """UPDATE cycle_detection
        SET is_assigned_to_trip = true
        WHERE bike_id = %s 
        AND sample_id <= %s
        """
        self.cur.execute(stmt, (bike_id, sample_id))


