import time
import psycopg2
import psycopg2.extras
import os

import park_event_analyzer
import trip_analyzer
import logging
import park_event_recover

conn_str = "dbname=deelfietsdashboard"

if "dev" in os.environ:
    conn_str = "dbname=deelfietsdashboard3"

if "ip" in os.environ:
    conn_str += " host={} ".format(os.environ['ip'])
if "password" in os.environ:
    conn_str += " user=deelfietsdashboard password={}".format(os.environ['password'])

conn = psycopg2.connect(conn_str)

# setup logging
format_string = '%(levelname)s: %(asctime)s: %(message)s'
logging.basicConfig(level=logging.DEBUG,format=format_string)

class Analyzer():
    def __init__(self, conn):
        self.conn = conn
        self.cur = conn.cursor(cursor_factory=psycopg2.extras.DictCursor)
        self.park_event_recoverer = park_event_recover.ParkEventRecover(self.cur)

    def derive_trips_for_bike_id(self, bike_id):
        stmt = """SELECT system_id, bike_id, ST_Y(location) as lat, ST_X(location) as lng, is_check_in,
        is_check_out, last_time_imported, sample_id, is_assigned_to_trip, is_assigned_to_park_event
        FROM cycle_detection
        WHERE bike_id = %s
        AND (is_assigned_to_trip = false or is_assigned_to_park_event = false)
        ORDER BY sample_id"""
        self.cur.execute(stmt, (bike_id,))

        records = self.cur.fetchall()
        trip_deriver = trip_analyzer.TripDeriver(self.cur)
        trip_deriver.analyze_records(records)

        park_event_deriver = park_event_analyzer.ParkEventAnalyzer(self.cur)
        park_event_deriver.analyze_records(records)


    def derive_trips(self):
        # Query all bike_ids of check_ins that are not assigned to a trip or all check_outs that are not assigned to to a park_event.
        stmt = """SELECT DISTINCT(bike_id) 
        FROM cycle_detection 
        WHERE (is_assigned_to_trip = false AND is_check_in = true)
        OR (is_assigned_to_park_event = false AND is_check_out = true)"""
        self.cur.execute(stmt)
        for row in self.cur.fetchall():
            self.derive_trips_for_bike_id(row[0])
        self.conn.commit()


    def loop(self):
        while True:
            start = time.time()
            logging.info("Recover park_events")
            self.park_event_recoverer.recover_park_events()
            self.conn.commit()
            logging.info("Completed recovering park_events")
            logging.info("Start derive_trips")
            self.derive_trips()
            logging.info("Completed derive_trips")
            self.conn.commit()
            
            time_spent = (time.time() - start)
            time_to_sleep = 60 - time_spent
            
            logging.info("time spent %.2f" % time_spent)
            if time_to_sleep > 0.0:
                time.sleep(time_to_sleep)


analyzer = Analyzer(conn)
analyzer.loop()
