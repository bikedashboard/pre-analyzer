import time

# For the location of a parking_event the latest location is used.
class ParkEvent():
    def __init__(self, system_id, bike_id, lat, lng):
        self.system_id = system_id
        self.bike_id = bike_id
        self.lat = lat
        self.lng = lng
        self.start_time = None
        self.end_time  = None
        self.check_out_sample_id = None
        self.park_event_id = None

    def register_start(self, cur, start_time, check_in_sample_id):
        self.start_time = start_time
        stmt = """INSERT INTO
        park_events
        (system_id, bike_id, location, start_time, check_in_sample_id)
        VALUES (%s, %s, ST_SetSRID(ST_Point(%s, %s), 4326), %s, %s)
        """
        cur.execute(stmt, (self.system_id, self.bike_id, self.lng, self.lat, self.start_time, check_in_sample_id))

    def register_end(self, cur, end_time, check_out_sample_id):
        if not self.check_has_start(cur):
            return
        result = self.get_current_parking_event_id(cur)
        self.park_event_id = result[0]
        self.start_time = result[1]
        self.check_out_sample_id = check_out_sample_id
        self.end_time = end_time
        self.update(cur)


    def update(self, cur):
        stmt = """UPDATE
            park_events
            SET system_id = %s,
            bike_id = %s, 
            location = ST_SetSRID(ST_Point(%s, %s), 4326), 
            start_time = %s, 
            end_time = %s,
            check_out_sample_id = %s
            WHERE park_event_id = %s"""
        cur.execute(stmt, (self.system_id, self.bike_id, self.lng, 
            self.lat, self.start_time, self.end_time, self.check_out_sample_id, self.park_event_id))


    # Check if previous check_in event is closed.
    def check_has_start(self, cur):
        stmt = """SELECT count(*)
            FROM park_events
            WHERE bike_id = %s
            AND end_time IS NULL;"""
        cur.execute(stmt, (self.bike_id,))
        number_of_starts = cur.fetchone()[0] 
        if number_of_starts > 1:
             print("\033[91m {}\033[00m" .format("To many starts: " + self.bike_id))
        return (number_of_starts != 0)

    def get_current_parking_event_id(self, cur):
        stmt = """SELECT park_event_id, start_time
            FROM park_events
            WHERE bike_id = %s
            AND end_time IS NULL"""
        cur.execute(stmt, (self.bike_id,))
        return cur.fetchone()
        
class ParkEventAnalyzer():
    def __init__(self, cur):
        self.cur = cur
        self.bike_id = None
        self.sample_id = None

    def analyze_records(self, records):
        for record in records:
            if record["is_assigned_to_park_event"] == True:
                continue

            # Overwrite trip when no movement is made.
            if record["is_check_out"]:
                park_event = ParkEvent(record["system_id"], record["bike_id"],
                    record["lat"], record["lng"])
                park_event.register_end(self.cur, record["last_time_imported"], record["sample_id"])
                self.set_samples_assigned(record["bike_id"], record["sample_id"])

            # This block is used to detect the first ever check_in of a bike
            # All other detections should be done with the first if block.
            if record["is_check_in"]:
                park_event = ParkEvent(record["system_id"], record["bike_id"],
                    record["lat"], record["lng"])
                park_event.register_start(self.cur, record["last_time_imported"], record["sample_id"])
                self.set_samples_assigned(record["bike_id"], record["sample_id"])

        self.update_samples_assigned()

    def set_samples_assigned(self, bike_id, sample_id):
        self.bike_id = bike_id
        self.sample_id = sample_id

    def update_samples_assigned(self):
        stmt = """UPDATE cycle_detection
        SET is_assigned_to_park_event = true
        WHERE bike_id = %s 
        AND sample_id <= %s
        """
        if self.sample_id and self.bike_id:
            self.cur.execute(stmt, (self.bike_id, self.sample_id))
